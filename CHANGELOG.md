This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Document Store Backend Connector Library for PostgreSQL

## [v1.0.1-SNAPSHOT]

- Enhanced accounting-postgresql-utilities range
- Removed usage of static variable 


## [v1.0.0]

- First Release
