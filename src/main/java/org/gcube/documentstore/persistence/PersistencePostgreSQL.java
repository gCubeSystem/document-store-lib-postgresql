/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.sql.Statement;

import org.gcube.accounting.utility.postgresql.PostgreSQLQuery;
import org.gcube.documentstore.records.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PersistencePostgreSQL extends PersistenceBackend {

	protected static final Logger logger = LoggerFactory.getLogger(PersistencePostgreSQL.class);

	protected StatementMap statementMap;

	@Override
	public void prepareConnection(PersistenceBackendConfiguration configuration) throws Exception {
		this.statementMap = new StatementMap(configuration);
	}

	@Override
	protected void openConnection() throws Exception {
		
	}

	@Override
	protected void reallyAccount(Record record) throws Exception {
		Statement statement = statementMap.getStatement(record);
		PostgreSQLQuery postgreSQLQuery = new PostgreSQLQuery();
		String sqlCommand = postgreSQLQuery.getSQLInsertCommand(record);
		statement.executeUpdate(sqlCommand);
	}
	
	public void insert(Record record) throws Exception {
		reallyAccount(record);
	}

	@Override
	protected void clean() throws Exception {
		statementMap.close();
	}

	@Override
	protected void closeConnection() throws Exception {
		statementMap.close();
	}

	public void commitAndClose() throws Exception {
		statementMap.close();
	}
	
	@Override
	public boolean isConnectionActive() throws Exception {
		return true;
	};

}