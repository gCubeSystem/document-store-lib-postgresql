/**
 * 
 */
package org.gcube.documentstore.persistence;

import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PersistencePostgreSQLTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(PersistencePostgreSQLTest.class);
		
	@Ignore
	@Test
	public void persistenceIsPostgreSQL() throws ObjectNotFound, Exception {
		logger.debug("Going to check if the Persistence is CouchBase");
		PersistenceBackendFactory.setFallbackLocation(null);
		String context = ContextTest.getCurrentContextFullName();
		FallbackPersistenceBackend fallbackPersistenceBackend = PersistenceBackendFactory.createFallback(context);
		
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.rediscoverPersistenceBackend(fallbackPersistenceBackend, context);
		Assert.assertTrue(persistenceBackend instanceof PersistencePostgreSQL);
	}
	
	@Ignore
	@Test
	public void testInsertRecords() throws ObjectNotFound, Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		String context = ContextTest.getCurrentContextFullName();
		FallbackPersistenceBackend fallbackPersistenceBackend = PersistenceBackendFactory.createFallback(context);
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.rediscoverPersistenceBackend(fallbackPersistenceBackend, context);
		Assert.assertTrue(persistenceBackend instanceof PersistencePostgreSQL);
		PersistencePostgreSQL persistencePostgreSQL = (PersistencePostgreSQL) persistenceBackend;
		for(int i=0; i<1; i++) {
			if(i%50000 == 0) {
				persistencePostgreSQL.commitAndClose();
			}
			
			ServiceUsageRecord serviceUsageRecord = TestUsageRecord.createTestServiceUsageRecord();
			AggregatedServiceUsageRecord aggregatedServiceUsageRecord = new AggregatedServiceUsageRecord(serviceUsageRecord);
			persistencePostgreSQL.insert(aggregatedServiceUsageRecord);
			
			/*
			UsageRecord usageRecord = getTestAggregatedJobUsageRecord();
			persistencePostgreSQL.insert(usageRecord);
			
			usageRecord = getTestAggregatedPortletUsageRecord();
			persistencePostgreSQL.insert(usageRecord);
			
			usageRecord = getTestAggregatedServiceUsageRecord();
			persistencePostgreSQL.insert(usageRecord);
			
			usageRecord = getTestAggregatedStorageStatusRecord();
			persistencePostgreSQL.insert(usageRecord);
			*/
			
		}
		persistencePostgreSQL.commitAndClose();
	}
	
	
}
